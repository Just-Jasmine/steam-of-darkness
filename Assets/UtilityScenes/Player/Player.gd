
extends Position3D
var desired_move_direction = Vector3()
var former_position = Vector3()
var facing_direction = 0
var floor_level = 3
var inputDisabled = false

onready var tween = $Tween

# 0 = passable, 1 = impassable, 2 = Stairs, 3 = surfable
var collision = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 1, 2, 2, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
var width = 30
var height = 30
# for the foot animation; once you walk one tile, one foot has moved; is only for animation
var foot = 0

enum Direction {
	DOWN,
	LEFT,
	RIGHT,
	UP
}


func _ready():
	former_position = translation


func _process(delta):
	if !inputDisabled:
		get_input()


func get_input(): 
	if Input.is_action_pressed("ui_down"):
		facing_direction = Direction.DOWN
	elif Input.is_action_pressed("ui_up"):
		facing_direction = Direction.UP
	elif Input.is_action_pressed("ui_left"):
		facing_direction = Direction.LEFT
	elif Input.is_action_pressed("ui_right"):
		facing_direction = Direction.RIGHT
	else: 
		set_idle_frame()
		return
	# disable input
	inputDisabled = true 

	move()


func move():
	set_process(false)

	desired_move_direction = Vector3.ZERO

	match facing_direction:
		0:
			desired_move_direction.z = 1
		1:
			desired_move_direction.x = -1
		2:
			desired_move_direction.x = 1
		3:
			desired_move_direction.z = -1
	# check for being on a stair
	if floor_level == 1.5: 
		if facing_direction == Direction.UP:
			desired_move_direction.y = 0.5
			# setting the floor_level to the next int: so if you were on floor_level 1 before the stair, it becomes 2
			floor_level = floor_level + 0.5 
		elif facing_direction == Direction.DOWN:
			desired_move_direction.y = -0.5
			# same as above, but last int: floor_level 2 becomes floor_level 1
			floor_level = floor_level - 0.5

	# check if the tile you want to move to is passable; if it is, continue
	if !collide():
		animate()

		# setting up the tween
		tween.interpolate_property(self, "translation", former_position, former_position + desired_move_direction, $AnimationPlayer.current_animation_length, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		tween.start()

		yield($AnimationPlayer, "animation_finished")

		# change what foot you are using
		if foot == 0:
			foot = 1
		else:
			foot = 0
	else:
		former_position = translation
		inputDisabled = false
		set_process(true)
		set_idle_frame()
		return

	former_position = translation
	inputDisabled = false
	set_process(true)


func set_idle_frame():
	$Sprite3D.frame = facing_direction * 4 # 0 = down, 4 = left, 8 = right, 12 = up


# animate function; starts animation based on foot
func animate():
	if foot == 0:
		match facing_direction:
			0:
				$AnimationPlayer.play("down") # down is an animation i made in godot
			1:
				$AnimationPlayer.play("left")
			2:
				$AnimationPlayer.play("right")
			3:
				$AnimationPlayer.play("up")
	elif foot == 1:
		match facing_direction:
			0:
				$AnimationPlayer.play("down2")
			1:
				$AnimationPlayer.play("left2")
			2:
				$AnimationPlayer.play("right2")
			3:
				$AnimationPlayer.play("up2")


# function to check if you are colliding with a wall
func collide():
	var x = translation.x + desired_move_direction.x + get_parent().translation.x - 0.5 # calculating the x value you will move to
	var z = translation.z + desired_move_direction.z + get_parent().translation.z - 0.25 # calculating the z value you will move to
	var index = width*z + x # calculating an index for the array at the top

	# this was just a test for ED (one of my team members)
	if x < 0.5:
		get_parent().get_child(1).get_child(6).frame = 2
	elif x < 11.5:
		get_parent().get_child(1).get_child(6).frame = 0
	else:
		get_parent().get_child(1).get_child(6).frame = 1

	# true = you collide, so you cant move, false = the opposite
	if index >= width * height or x > width - 1 or x < 0 or z < 0: # failsafe for out of bounds
		return true
	if collision[index] == 0: # explained at the top
		return false
	elif collision[index] == 2: # stairs
		if facing_direction == Direction.UP:
			desired_move_direction.y = 0.5
			floor_level = 1.5
		elif facing_direction == Direction.DOWN:
			desired_move_direction.y = -0.5
			floor_level = 1.5
		return false
	else:
		return true
	pass
